import React from 'react';


const Posts = ({ posts, loading }) => {
  if (loading) {
    return <h2>Loading...</h2>;
  }

  return (
    // <ul className="list-group mb-4">
    //   {posts.map(post => (
    //     <li key={post.id} className="list-group-item" >
    //         {post.title}
    //     </li>
    //   ))}
    // </ul>
    <table className='table'>
        <thead>
            <tr>
                <th scope='col'>ID</th>
                <th scope='col'>User ID</th>
                <th scope='col'>Title</th>
            </tr>
        </thead>
        
        <tbody>           
            {posts.map(posts => (
               <tr>   
                <td>{posts.id}</td>
                <td>{posts.userId}</td>
                <td>{posts.title}</td>
                </tr>    
            
                // <td>{posts.userId}</td>
                // <td>{posts.title}</td>
            ))}        
            
        </tbody>
        
        
    </table>


  );
};

export default Posts;
