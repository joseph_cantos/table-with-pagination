import React from 'react';

import { Column, Table } from 'react-virtualized';
// import 'react-virtualized/styles.css'; // only needs to be imported once

// Table data as an array of objects
const list = [
  { name: 'Brian Vaughn', description: 'Software engineer' }
  
];

// Render your table

const Virtualizedtable = ({posts, loading}) => {

  console.log(posts, loading);

  if(loading) {
    return <h2>Loading....</h2>
  }

return (
  <Table
  width={300}
  height={300}
  headerHeight={20}
  rowHeight={30}
  rowCount={posts.length}
  rowGetter={({ index }) => posts[index]}
>
  <Column
    label={posts.id}
    dataKey={posts.id}
    width={100}
  />
  <Column
    width={200}
    label={posts.userId}
    dataKey={posts.userId}
  />
  <Column
      width={200}
      label={posts.title}
      dataKey={posts.title}
    />

</Table>

  )  

} 
export default Virtualizedtable