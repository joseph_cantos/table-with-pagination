import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Posts from './components/Posts'
import Pagination from './components/Pagination';
import Virtualtable from './components/Virtualtable';


function App() {

  const [posts, setPosts] = useState([]);
  const [loading,setLoading] = useState(false); //this is for API / fetching
  const [currentPage, setCurrentPage] = useState(1); //this is for page pagination
  const [postsPerPage] = useState(5); //this is for the amount of data per page


  //this is a compoonentDidMount / componentDidUpdate
  useEffect(()=> {
    const fetchPosts = async () => {
      setLoading(true);
      const res = await axios.get('https://jsonplaceholder.typicode.com/posts');
      setPosts(res.data);
      setLoading(false);
    }
    fetchPosts();
  }, []) //second argument is to stop updating the components

  
  console.log(posts);


  //Show 
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPost = posts.slice(indexOfFirstPost, indexOfLastPost);

  //Change page
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  console.log(currentPost)
  return (
    <div className="container mt-5"> 
      <h1>Basic Table Implementation with Pagination</h1>
      {/* <Virtualtable posts={currentPost} loading={loading}/> */}
      <Posts posts={currentPost} loading={loading}/>
      
      <Pagination postsPerPage={postsPerPage} totalPosts={posts.length} paginate={paginate}/>

      
    </div>
  );
}

export default App;
